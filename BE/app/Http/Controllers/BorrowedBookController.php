<?php

namespace App\Http\Controllers;

use App\Models\BorrowedBook;
use App\Models\Book;
use App\Models\ReturnedBook;
use App\Http\Requests\BorrowedBookRequest;
use Illuminate\Http\Request;

class BorrowedBookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $borrowedbook = BorrowedBook::all();
        return response()->json([
            "message" => "Borrowed Books",
            "data" => $borrowedbook]);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(BorrowedBookRequest $request)
    {
        $bbook = new BorrowedBook();
        $bbook->copies = $request->copies;
        $bbook->book_id = $request->book_id;
        $bbook->patron_id = $request->patron_id;

        $books = Book::find($bbook->book_id);
    
        if($bbook->copies > $books->copies){
            return response()->json(
                ["message" => "Stocks available cannot meet your input"]);
            }
 
        else{
        $minuscopies = $books->copies - $bbook->copies;
        
        $validated = $request->validated();

        $bbook->save();
        $books->update(['copies' => $minuscopies]);
        return response()->json(
               ["message" => "Borrow book Success",
               "data" => $bbook, $books]);
            }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $bbook = BorrowedBook::find($id);
        return response()->json($bbook);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(BorrowedBookRequest $request, $id)
    {
        $bbook = BorrowedBook::find($id);
        $rbook = new ReturnedBook();
        
        $rbook->copies = $request->copies;
        $rbook->book_id = $request->book_id;
        $rbook->patron_id = $request->patron_id;

        $book = Book::find($rbook->book_id);
        $validated = $request->validated();

        if($bbook->copies == $rbook->copies) {
            $bbook->delete();
        }
        if($bbook->copies < $rbook->copies) {
            return response()->json(
                ["message" => "The number of book(s) to Return is Invalid"]);
        }


        $addcopiestobook = $book->copies + $rbook->copies;
        $bbookcopies =  $bbook->copies - $rbook->copies;
    
        $rbook->save();
        $bbook->update(['copies' => $bbookcopies]);
        $book->update(['copies' => $addcopiestobook]);
        return response()->json(
               ["message" => "Book is Returned",
               "data" => $bbook,$book, $rbook]);
    }

}
