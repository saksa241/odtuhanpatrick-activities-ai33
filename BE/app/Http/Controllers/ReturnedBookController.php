<?php

namespace App\Http\Controllers;

use App\Models\ReturnedBook;
use Illuminate\Http\Request;

class ReturnedBookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $returnedbook = ReturnedBook::all();
        return response()->json([
            "message" => "Returned Books",
            "data" => $returnedbook]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $rbook = ReturnedBook::find($id);
        return response()->json($rbook);
    }

}
