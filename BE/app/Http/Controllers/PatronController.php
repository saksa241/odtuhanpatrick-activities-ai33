<?php

namespace App\Http\Controllers;

use App\Models\Patron;
use App\Http\Requests\PatronRequest;
use Illuminate\Http\Request;

class PatronController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $patron = Patron::all();
        return response()->json([
            "message" => "Patron",
            "data" => $patron]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PatronRequest $request)
    {
        $patron = new Patron();

        $patron->last_name = $request ->last_name;
        $patron->first_name = $request ->first_name;
        $patron->middle_name = $request ->middle_name;
        $patron->email = $request ->email;

        $validated = $request->validated();

        $patron->save();
        return response()->json($patron);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PatronRequest $request, $id)
    {
        $patron = Patron::find($id);

        $patron->last_name = $request->input('last_name');
        $patron->first_name = $request->input('first_name');
        $patron->middle_name = $request->input('middle_name');
        $patron->email = $request->input('email');

        $validated = $request->validated();

        $patron->update();
        return response()->json($patron);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $patron = Patron::find($id);
        $patron->delete();
        return response()->json($patron);
    }
}
