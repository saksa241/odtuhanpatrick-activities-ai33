<?php

namespace Database\Seeders;

use App\Models\Category;
use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
    $multiple_row = [
    [ 'category' => 'Fiction'],
    [ 'category' => 'Biography'],
    [ 'category' => 'Romance'],
    [ 'category' => 'School'],
    [ 'category' => 'Horror'],    
    ];   

    foreach ($multiple_row as $rows) {
        Category::create($rows);
    }
 }
}
