import { mapGetters, mapActions } from "vuex";
export default {
  data() {
    return {
      Form: {
        name: "",
        author: "",
        copies: "",
        category_id: "",
      },
      PatronModel: [],
      SelectedPatron: "",
      Search: "",
    };
  },
  methods: {
    ...mapActions([
      "fetchCategory",
      "fetchBooks",
      "addBooks",
      "updateBooks",
      "removeBooks",
    ]),
    AddBook() {
      if (this.Form.BookName == "" || this.Form.Author == "") {
        this.ErrorToast();
      } else {
        this.addBooks(this.Form);
        console.log(this.Form);
        this.HideModal();
        this.AddToast();
      }
    },
    UpdateBook(index) {
      console.log(index);
      this.updateBooks(this.Form);

      this.HideModal();
    },
    ToBeUpdated(index) {
      this.SelectedPatron = index;
      this.Form.BookName = this.PatronModel[index].BookName;
      this.Form.Author = this.PatronModel[index].Author;
      this.Form.Genre = this.PatronModel[index].Genre;
    },
    DeleteBook() {
      this.removeBooks(this.SelectedPatron);

      this.HideModal();
    },
    HideModal() {
      this.$refs.modal.hide();
      this.FormReset();
    },
    FormReset() {
      (this.Form = {
        name: "",
        author: "",
        copies: "",
        category_id: "",
      }),
        (this.SelectedPatron = "");
    },
    AddToast() {
      this.$toast.success(`Successfully Added`, {
        position: "top-center",
        timeout: 3000,
        closeOnClick: true,
        pauseOnFocusLoss: false,
        pauseOnHover: false,
        draggable: true,
        draggablePercent: 0.6,
        showCloseButtonOnHover: false,
        hideProgressBar: true,
        closeButton: "button",
        icon: true,
        rtl: false,
      });
    },
    ErrorToast() {
      this.$toast.error(`Please Fill the Data`, {
        position: "top-center",
        timeout: 3000,
        closeOnClick: true,
        pauseOnFocusLoss: false,
        pauseOnHover: false,
        draggable: true,
        draggablePercent: 0.6,
        showCloseButtonOnHover: false,
        hideProgressBar: true,
        closeButton: "button",
        icon: true,
        rtl: false,
      });
    },
  },
  created() {
    this.fetchBooks();
    this.fetchCategory();
  },
  computed: {
    ...mapGetters(["AllBooks", "AllCategories"]),
    PatronModelFilter() {
      return this.AllBooks.filter((Book) => {
        return this.Search.toLowerCase()
          .split(" ")
          .every((v) => Book.name.toLowerCase().includes(v));
      });
    },
  },
};